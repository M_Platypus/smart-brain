import React, { Component } from 'react'


export default class InputBox extends Component {
    render () {
        const { label, onNameChange, onEmailChange, onPasswordChange } = this.props;
        return (
            <div>
            { this.props.label === 'Name' ?
            <div className="mt3">
                <label className="db fw6 lh-copy f6" htmlFor="name">{label}</label>
                <input
                className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                type="text"
                name="name"
                id="name"
                onChange={onNameChange}
                />
            </div>
            : label === 'Email' ?
            <div className="mt3">
            <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
            <input
                className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                type="email"
                name="email-address"
                id="email-address"
                onChange={onEmailChange}
            />
            </div>
            : <div className="mv3">
                <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                <input
                  className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                  type="password"
                  name="password"
                  id="password"
                  onChange={onPasswordChange}
                />
              </div>
            }
            </div>
        );
    };
};