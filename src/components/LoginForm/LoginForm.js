import React, { Component } from 'react'
import InputBox from './InputBox'

export default class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            signInEmail: '',
            signInPassword: '',
            email: '',
            password: '',
            name: ''
        }
    }
    onRNameChange = (event) => {
    this.setState({name: event.target.value})
  }

    onREmailChange = (event) => {
        this.setState({email: event.target.value})
    }

    onRPasswordChange = (event) => {
        this.setState({password: event.target.value})
    }

    onEmailChange = (event) => {
    this.setState({signInEmail: event.target.value})
    }

    onPasswordChange = (event) => {
        this.setState({signInPassword: event.target.value})
    }

    onSubmitSignIn = () => {
        fetch('https://frozen-sands-39147.herokuapp.com/signin', {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            email: this.state.signInEmail,
            password: this.state.signInPassword
        })
        })
        .then(response => response.json())
        .then(user => {
            if (user.id) {
            this.props.loadUser(user)
            this.props.onRouteChange('home');
            }
        })
    }

    onSubmitRegister = () => {
        fetch('https://frozen-sands-39147.herokuapp.com/register', {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            email: this.state.email,
            password: this.state.password,
            name: this.state.name
        })
        })
        .then(response => response.json())
        .then(user => {
            if (user.id) {
            this.props.loadUser(user)
            this.props.onRouteChange('home');
            }
        })
    }

    render() {
        const { loginType, onRouteChange } = this.props;
        const { onRNameChange, onREmailChange, onRPasswordChange, onPasswordChange, onEmailChange } = this;
        return (
            <article className="br3 ba b--black-10 mv4 w-100 w-50-m w-25-l mw6 shadow-4 center">
                <main className="pa4 black-80">
                <div className="measure">
                <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                <legend className="f1 fw6 ph0 mh0">{loginType}</legend>
                {loginType === 'Sign In' ?
                <div>
                    <InputBox
                    onEmailChange={onEmailChange}
                    label='Email'
                    />
                    <InputBox
                    onPasswordChange={onPasswordChange}
                    label='Password'
                    />
                </div>
                : <div>
                    <InputBox
                    onNameChange={onRNameChange}
                    label='Name'
                    />
                    <InputBox
                    onEmailChange={onREmailChange}
                    label='Email'
                    />
                    <InputBox
                    onPasswordChange={onRPasswordChange}
                    label='Password'
                    />
                </div>}
                </fieldset>
                {loginType === 'Sign In' ?
                <div>
                    <div className="">
                        <input
                            onClick={this.onSubmitSignIn}
                            className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib"
                            type="submit"
                            value="Sign in"
                        />
                    </div>
                    <div className="lh-copy mt3">
                        <p  onClick={() => onRouteChange('register')} className="f6 link dim black db pointer">
                            Register
                        </p>
                    </div>
                </div>
                : <div>
                    <div className="">
                        <input
                            onClick={this.onSubmitRegister}
                            className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib"
                            type="submit"
                            value="Register"
                        />
                    </div>
                    <div className="lh-copy mt3">
                        <p  onClick={() => onRouteChange('signin')} className="f6 link dim black db pointer">
                            Sign In
                        </p>
                    </div>
                </div>
                }
                </div>
                </main>
            </article>
        )
    }
}



/* Stuff this compenent needs.

/ Props
> state

/Stuff thats the same
>onEmailChange
>onPasswordChange
*/