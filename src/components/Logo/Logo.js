import React from 'react'
import Tilt from 'react-tilt'
import AIMind from './AIMindMap.png'
import'./Logo.css'


const Logo = () => {
    return (
        <div className='ma4 mt0'>
            <Tilt className="Tilt br2 shadow-2" options={{ max : 45 }} style={{ height: 100, width: 100 }} >
                <div className="Tilt-inner pa3"> 
                    <img style = {{paddingTop: '5px'}} alt='Logo' src={AIMind}/>  
                </div>
            </Tilt>
        </div>
    )
}

export default Logo